set number
set relativenumber

set laststatus=2
set showtabline=2
set t_Co=256

colorscheme typewriter-night
